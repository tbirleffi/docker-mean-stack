#!/bin/bash

if [ ! -f ".installed" ]; then
	echo "The MEAN Stack is not installed yet, run: (docker-compose run mean init)";
	exec mean "$@"
else
	echo "The MEAN Stack is installed...";
	cd www;
	exec mean "$@";
fi