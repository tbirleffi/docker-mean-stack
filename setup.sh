#!/bin/bash

ENV="$1";
PRJ="$2";
DIR="data";
CUR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd );
COP="docker-compose.yml";
NAME="mean-stack.com";
source ~/.bash_profile;

runDockerComposeSetup()
{
	if [ "$ENV" = "" ]
	then
		echo "Please provide the environment: local, dev, stage, prod, jenkins";
		exit 1;
	fi

	if [[ "$PRJ" = "" && "$ENV" != "local" ]]
	then
		echo "Please provide the environment project name: (ex: MyProjDev)";
		exit 1;
	fi

	if [ "$ENV" != "" ]
	then
		copyDockerComposeFile;
		runServices;
	fi
}

listServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" ps;
	else
		docker-compose ps;
	fi
}

startServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" up;
	else
		docker-compose up -d;
	fi
}

stopServices()
{
	if [ -e "$COP" ]
	then
		if [ "$PRJ" != "" ]
		then
			docker-compose -p "$PRJ" stop;
		else
			docker-compose stop;
		fi
	fi
}

copyDockerComposeFile()
{
	cp "env/$ENV/docker-compose.yml" "$COP";
}

runLocalNetworkSetup()
{
	echo Starting boot2docker if not already started;
	boot2docker status || boot2docker up;

	export the_ip=$(boot2docker ip 2>&1 | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");
	export old_route_ip=$(netstat -r | grep 172.17  | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");
	echo Found boot2docker ip $the_ip;

	if [ "$the_ip" == "" ]; then
		echo Could not find the ip of boot2docker.. =/;
		exit;
	fi

	echo Seting up routes. Enter sudo password;
	if [ "$old_route_ip" != "" ]; then
		sudo route -n delete -net 172.17.0.0 $old_route_ip;
	fi
	sudo route -n add -net 172.17.0.0 $the_ip;

	echo Setting up hosts;
	cp /etc/hosts hosts;
	grep -v '# boot2dockerscriptmeanstack' hosts > hosts_temp;
	mv hosts_temp hosts;
	for containerid in $(docker ps -q); do
		export domain=$(docker inspect $containerid | grep '"Domainname":' | cut -f2 -d":" | cut -f2 -d'"');
		export host=$(docker inspect $containerid | grep '"Hostname":' | cut -f2 -d":" | cut -f2 -d'"');
		export ip=$(docker inspect $containerid | grep 'IPAdd' | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");
		
		if [[ "$domain" != "" && $domain == *"$NAME"* && "$ENV" != "jenkins" ]]; then
			env=`echo $domain| cut -d '.' -f 1`;
			echo Storing values $ip $host.$domain;
			echo $ip $host $domain \# boot2dockerscriptmeanstack >> hosts;
		fi
	done
	sudo mv hosts /etc/hosts;
}

copyExtraFiles()
{
	if [ -d "$DIR/www" ]; then
		cp "env/.installed" "$DIR/.installed";
		cp "env/$ENV/config/development.js" "$DIR/www/config/env/development.js";
		cp "env/$ENV/config/test.js" "$DIR/www/config/env/test.js";
		cp "env/$ENV/gulp/development.js" "$DIR/www/gulp/development.js";
		cp "env/$ENV/gulp/test.js" "$DIR/www/gulp/test.js";
		cp "env/global/bson-fix.js" "$DIR/www/node_modules/meanio/lib/core_modules/server/node_modules/connect-mongo/node_modules/mongodb/node_modules/bson/ext/index.js";
		cp "env/global/bson-fix.js" "$DIR/www/node_modules/mongoose/node_modules/mongodb/node_modules/bson/ext/index.js";
		cp "env/global/full.gitignore" "$CUR/.gitignore";
	fi
}

runServices()
{
	stopServices;
	startServices;
	listServices;
}

runSetupCommands()
{
	if [[ -d "$DIR/www" && ! -f "$DIR/.installed" ]]; then
		rm -rf $DIR/www;
	fi
	
	if [ "$(ls -A $DIR/www)" ]; then
		#docker-compose run bower update --allow-root;
		echo "Starting MEAN services...";
		copyExtraFiles;
	else
		# Or, you can pull in from your own project repo at this point too.
		docker-compose run mean init www;
		docker-compose run npm install;
		docker-compose run bower install --allow-root;
		copyExtraFiles;
		runServices;
	fi
}

runGulpLocal()
{
	cd $DIR/www;
	gulp;
}

runDockerComposeSetup;
runSetupCommands;
runLocalNetworkSetup;
runGulpLocal;
